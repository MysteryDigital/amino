<?php

$app = require('bootstrap/start.php');

try {
    $filesToProcess = glob('input_files/*');

    if (!is_array($filesToProcess) || count($filesToProcess) == 0) {
        throw new App\Exceptions\NoFilesException();
    }

    foreach ($filesToProcess as $file) {

        $pathParts = pathinfo($file);

        if (is_array($pathParts) && isset($pathParts['extension'])) {

            $extension = $pathParts['extension'];

            // we would add to the switch for more strategies based on the file extension
            switch (strtolower($extension)) {
                case 'xml' :
                    $app->setStrategy(new App\ImportStrategies\XmlStrategy($container));
                    break;
                default :
                    $container['logger']->debug("Skipping [$file] because I can't find a strategy to use to import it");
                    continue;
            }

            $app->processFile(__DIR__ . '/' . $file);

        } else {

            $container['logger']->debug("Skipping [$file] because I can't find the extension");

            continue;
        }

        $container['logger']->debug("Script finished.  Peak memory usage was [" . memory_get_peak_usage (true) ."]");

    }

} catch (App\Exceptions\NoFilesException $e) {
    echo "I could not find any files to process" . PHP_EOL;
}

