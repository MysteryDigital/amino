<?php

require('bootstrap/start.php');

try {
    $dbHelper = new App\Domain\DatabaseHelper($container);

    $dbHelper->dropSchema();

    $dbHelper->setupSchema();

    $dbHelper->seedChannel(__DIR__ . '/database/seeds/channel_seeds.csv');

    echo 'Ok.  Database tables were dropped and recreated' . PHP_EOL;

} catch (Exception $e) {

    $container['logger']->err("Fatal error trying to refresh the database [{$e->getMessage()}]");

    echo "Fatal error trying to refresh the database [{$e->getMessage()}]" . PHP_EOL;

}



