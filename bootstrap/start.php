<?php

require(__DIR__ . '/../vendor/autoload.php');

// setup the IoC container
require('ioc.php');

// setup the database ORM
require('database.php');

// bootstrap the app
return $app = new App\Amino($container);