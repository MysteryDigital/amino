<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$config = $container['config'];

$capsule->addConnection([
    'driver'    => $config->get('database.driver'),
    'host'      => $config->get('database.host'),
    'database'  => $config->get('database.db_name'),
    'username'  => $config->get('database.user'),
    'password'  => $config->get('database.pass'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();

$capsule->bootEloquent();
