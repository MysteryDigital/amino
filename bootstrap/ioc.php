<?php

/**
 *  We're using Pimple as our IoC container.
 *  This script sets it up and defines the dependencies (services) that we
 *  will be needing elsewhere.
 */

use Pimple\Container;
use Noodlehaus\Config;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;

// the pimple IoC container
$container = new Container();

// this allows a convenient central place to maintain a config
// You can see examples of working config files at https://github.com/hassankhan/config/tree/master/tests/mocks/pass
$container['config'] = function ($container) {
    return new Config(__DIR__ . '/../config/');
};

// We need a logging function, make sure that the object you use conforms to PSR3 if you swap monolog out
$container['logger'] = function ($container) {

    $logPath = __DIR__ . '/../logs/';

    $handler = new Monolog\Handler\RotatingFileHandler($logPath . 'app.log');

    $formatter = new Monolog\Formatter\LineFormatter("[%datetime%] %level% : %message% %context% %extra%\r\n", null, false, true);

    $handler->setFormatter($formatter);

    $log = new Monolog\Logger('main');

    $log->pushHandler($handler);

    return $log;
};

$container['channelDataService'] = function ($container) {
    return new App\Domain\Services\ChannelDataService($container);
};

$container['channelDataRepository'] = function ($container) {
    return new App\Domain\Repositories\ChannelDataRepository($container);
};

$container['programDataRepository'] = function ($container) {
    return new App\Domain\Repositories\ProgramDataRepository($container);
};

$container['scheduleDataRepository'] = function ($container) {
    return new App\Domain\Repositories\ScheduleDataRepository($container);
};