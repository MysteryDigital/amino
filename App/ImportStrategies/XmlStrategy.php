<?php

namespace App\ImportStrategies;

use DateTime;
use Pimple\Container;

class XmlStrategy implements \App\Interfaces\ImportStrategyInterface
{
    const CHANNEL_TAG_NAME = 'service';

    /**
     * @var mixed
     */
    private $channelDataRepository;

    /**
     * @var Noodlehaus\Config
     */
    private $config;

    /**
     * @var Monolog\Logger;
     */
    private $logger;

    /**
     * XmlStrategy constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container['logger'];

        $this->channelDataRepository = $container['channelDataRepository'];

        $this->programDataRepository = $container['programDataRepository'];

        $this->config = $container['config'];

    }

    /**
     * Returns an array containing data that needs to be inserted into the program table.
     *
     * @param \SimpleXMLElement $program
     * @return array
     */
    private function buildProgramRow(\SimpleXMLElement $program)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        if (isset($program->short_event)) {
            // program data are available
            $programDetails = $program->short_event->attributes();

            // convert the iso3 language to iso2, if it is present
            if (isset($programDetails->language)) {

                $iso3 = strtoupper((string)$programDetails->language);

                $countryCodes = array_flip($this->config->get('country_codes'));

                if (isset($countryCodes[$iso3])) {

                    $iso2Lang = $countryCodes[$iso3];

                }
            }

            $dataRow = [
                'type'    => 'program',
                'payload' => [
                    'ext_program_id' => mt_rand(10000000, 99999999), // sorry, i can't spot which attribute to use in the data
                    'show_type'      => isset($programDetails->show_type) ? $programDetails->show_type : null,
                    'long_title'     => isset($programDetails->name) ? $programDetails->name : null,
                    'grid_title'     => isset($programDetails->grid_title) ? $programDetails->grid_title : null,
                    'original_title' => isset($programDetails->original_title) ? $programDetails->original_title : null,
                    'duration'       => isset($programDetails->duration) ? $programDetails->duration : null,
                    'iso_2_lang'     => isset($iso2Lang) ? $iso2Lang : null,
                    'eidr_id'        => isset($programDetails->eidr_id) ? $programDetails->eidr_id : null,
                ],
            ];

            foreach ($dataRow['payload'] as $key => $value) {
                // cast to string from the simplexml element type
                $dataRow['payload'][$key] = (string)$value;

                if (empty($dataRow['payload'][$key])) {
                    $dataRow['payload'][$key] = null;
                }
            }

        } else {
            // the provider does not supply program data
            $dataRow = [];
        }

        return $dataRow;
    }

    /**
     * Builds and returns an array to be inserted as a row in the schedule database
     *
     * @param \SimpleXMLElement $event
     * @param $channelId
     * @return array
     */
    private function buildScheduleRow(\SimpleXMLElement $event, $channelId, $programId)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        $attributes = (array)$event->attributes();

        $attributes = $attributes['@attributes'];

        if (isset($attributes['start_time'])) {
            // this field is in the format "16/06/02 01:00:00"
            $startTime = DateTime::createFromFormat('d/m/y H:i:s', $attributes['start_time']);
        }

        if (isset($attributes['duration'])) {
            // xml duration is in the form 02:25:00
            $timeParts = explode(':', $attributes['duration']);

            $intervalString = "PT" . $timeParts[0] . 'H' . $timeParts[1] . 'M' . $timeParts[2] . 'S';

            $duration = new \DateInterval($intervalString);

            $runTime = $duration->format('%s');

            $runTime += $duration->format('%i') * 60;

            $runTime += $duration->format('%H') * 3600;
        }

        if (isset($duration) && isset($startTime)) {

            $endTime = clone($startTime);

            $endTime->add($duration);

        }

        $dataRow = [
            'type'    => 'schedule',
            'payload' => [
                'ext_schedule_id' => mt_rand(10000000, 99999999), // sorry, i can't spot which attribute to use in the data
                'channel_id'      => $channelId,
                'start_time'      => isset($startTime) ? $startTime->format('U') : null,
                'end_time'        => isset($endTime) ? $endTime->format('U') : null,
                'run_time'        => isset($runTime) ? $runTime : null,
                'program_id'      => $programId,
                'is_live'         => $attributes['running_status'],
            ],
        ];

        return $dataRow;
    }

    /**
     * Generator function that returns rows.
     * The data array returned must indicate which table the row is for so that we can handle it properly.
     *
     * @param $filename
     */
    public function generateDatabaseRow($filename)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        // limit our memory usage to a single channel at a time by using XMLReader
        $xmlReader = new \XMLReader();

        if (false === $xmlReader->open($filename)) {
            $this->logger->err(__METHOD__ . " : XML file [$filename] could not be opened");
            throw new \DomainException("XML file could not be opened");
        }

        // find first service node (which is a channel)
        while ($xmlReader->read() && $xmlReader->name != self::CHANNEL_TAG_NAME) ;

        while ($xmlReader->name == self::CHANNEL_TAG_NAME) {

            $node = new \SimpleXMLElement($xmlReader->readOuterXML());

            $attributes = $node->attributes();

            $channel = $this->channelDataRepository->getChannel($attributes['id']);

            if (is_null($channel)) {

                $this->logger->debug(__METHOD__ . " : Skipping channel [{$attributes['id']}] because it is not in our database");

            } else {

                $this->logger->debug(__METHOD__ . " : Processing channel [{$attributes['id']}");

                echo ".";

                $scheduleEvents = $node->children();

                foreach ($scheduleEvents as $event) {

                    foreach ($event->children() as $program) {

                        $row = $this->buildProgramRow($program);

                        if (is_array($row) && !empty($row)) {

                            $existingRow = $this->programDataRepository->getProgram(['long_title', $row['payload']['long_title']]);

                            if (is_null($existingRow)) {
                                // only yield the row to be inserted if it doesn't exist already
                                yield($row);

                                // fetch the row we just inserted, a little slow but we can't pass it back into the generator
                                $existingRow = $this->programDataRepository->getProgram(['long_title', $row['payload']['long_title']]);

                            }

                            $programId = $existingRow->id;

                            yield($this->buildScheduleRow($event, $channel->id, $programId));

                        } // if empty row

                    } // foreach children

                } // foreach schedule events

            } // if null

            // move to the next service/channel node
            while ($xmlReader->read() && $xmlReader->name != self::CHANNEL_TAG_NAME) ;
        } // while

    }


}