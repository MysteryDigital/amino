<?php

namespace App\Interfaces;

interface ImportStrategyInterface
{
    /**
     * Generator function that returns rows.
     * The data array returned must indicate which table the row is for so that we can handle it properly.
     *
     * @param $filename
     */
    public function generateDatabaseRow($filename);
}