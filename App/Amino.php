<?php

namespace App;

use Pimple\Container;

class Amino
{
    /**
     * @var Noodlehaus\Config
     */
    private $config;

    /**
     * @var Monolog\Logger;
     */
    private $logger;

    /**
     * Concrete implementation of App\Interfaces\ImportStrategyInterface
     * @var object
     */
    private $importStrategy;

    /**
     * Amino constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container['logger'];

        $this->logger->debug(__METHOD__ . ' : Application initialized');

        $this->config = $container['config'];

        $this->channelDataService = $container['channelDataService'];

        $this->channelDataRepository = $container['channelDataRepository'];

        $this->programDataRepository = $container['programDataRepository'];

        $this->scheduleDataRepository = $container['scheduleDataRepository'];

    }

    /**
     * Inserts a row of data into the correct table using the type key to identify which table to use
     * @param array $row
     */
    public function insertDatabaseRow(array $row)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        switch ($row['type']) {
            case 'program' :
                try {
                    $this->programDataRepository->insertRow($row['payload']);
                } catch (Exception $e) {
                    $this->logger->debug(__METHOD__ . " : Error inserting program row [{$e->getMessage()}]");
                }
                break;
            case 'schedule' :
                try {
                    $this->scheduleDataRepository->insertRow($row['payload']);
                } catch (Exception $e) {
                    $this->logger->debug(__METHOD__ . " : Error inserting schedule row [{$e->getMessage()}]");
                }
                break;
            default :
                throw new \InvalidArgumentException('Payload type not recognized');
        }

    }

    /**
     * Attempts to process the given file
     * @param $filename
     * @return bool
     */
    public function processFile($filename)
    {
        $this->logger->debug(__METHOD__ . " : bof [$filename]");

        foreach ($this->importStrategy->generateDatabaseRow($filename) as $row) {

            $this->insertDatabaseRow($row);

        }
    }

    /**
     * Defines which strategy we'll use to import the file.
     * @param App\Interfaces\ImportStrategyInterface $strategy
     */
    public function setStrategy(\App\Interfaces\ImportStrategyInterface $strategy)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        $this->importStrategy = $strategy;
    }
}