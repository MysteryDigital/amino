<?php

namespace App\Domain\Services;

use Pimple\Container;

class ChannelDataService
{

    /**
     * ChannelDataService constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container['logger'];

    }

}