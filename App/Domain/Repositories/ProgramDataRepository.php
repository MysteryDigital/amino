<?php

namespace App\Domain\Repositories;

use App\Domain\Entities\ProgramModel;
use Pimple\Container;

class ProgramDataRepository
{
    /**
     * ChannelDataService constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container['logger'];
    }

    /**
     * Fetch the row for the program.
     * Pass a single-dimension array with two elements, the first is key and the second is value
     * @param array $keyValue
     * @return mixed
     */
    public function getProgram(array $keyValue)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        if (count($keyValue) !== 2) {
            // this should be caught at develop time, and so giving an error message that is meaningless to the front user is okay
            throw new \InvalidArgumentException('Pass a single-dimension array with two elements, the first is key and the second is value');
        }

        return ProgramModel::where($keyValue[0], $keyValue[1])->get()->first();
    }

    /**
     * Inserts a new row into the database
     * @param array $rowData
     * @return mixed
     */
    public function insertRow(array $rowData)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        $entity = new ProgramModel();

        foreach ($rowData as $key => $value) {
            $entity->$key = $value;
        }

        return $entity->save();
    }
}