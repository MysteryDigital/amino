<?php

namespace App\Domain\Repositories;

use App\Domain\Entities\ChannelModel;
use Pimple\Container;

class ChannelDataRepository
{
    /**
     * ChannelDataService constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container['logger'];
    }

    /**
     * Fetch the row for the channel out of the database
     * @param int $serviceId
     * @return mixed
     */
    public function getChannel($serviceId)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        return ChannelModel::where('source_id', $serviceId)->get()->first();
    }
}
