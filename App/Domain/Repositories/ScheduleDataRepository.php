<?php

namespace App\Domain\Repositories;

use App\Domain\Entities\ScheduleModel;
use Pimple\Container;

class ScheduleDataRepository
{
    /**
     * ChannelDataService constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container['logger'];
    }

    /**
     * Inserts a new row into the database
     * @param array $rowData
     * @return mixed
     */
    public function insertRow(array $rowData)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        $entity = new ScheduleModel();

        foreach ($rowData as $key => $value) {
            $entity->$key = $value;
        }

        return $entity->save();
    }
}