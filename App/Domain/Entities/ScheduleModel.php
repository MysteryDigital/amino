<?php

namespace App\Domain\Entities;

use Illuminate\Database\Eloquent\Model;

class ScheduleModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_livetv_schedule';
}