<?php

namespace App\Domain\Entities;

use Illuminate\Database\Eloquent\Model;

class ProgramModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_livetv_program';
}