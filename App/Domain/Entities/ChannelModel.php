<?php

namespace App\Domain\Entities;

use Illuminate\Database\Eloquent\Model;

class ChannelModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_livetv_channel';
}