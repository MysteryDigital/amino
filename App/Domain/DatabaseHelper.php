<?php

namespace App\Domain;

use App\Domain\Entities\ChannelModel;
use Illuminate\Database\Capsule\Manager as Capsule;
use Pimple\Container;

class DataBaseHelper
{
    /**
     * @var Monolog\Logger;
     */
    private $logger;

    /**
     * DataBaseHelper constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->logger = $container['logger'];
    }

    /**
     * Drops the tables
     */
    public function dropSchema()
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        Capsule::schema()->disableForeignKeyConstraints();

        Capsule::schema()->dropIfExists('service_livetv_channel');

        Capsule::schema()->dropIfExists('service_livetv_program');

        Capsule::schema()->dropIfExists('service_livetv_schedule');

        Capsule::schema()->enableForeignKeyConstraints();
    }

    public function seedChannel($filename)
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        if (false == $handle = fopen($filename, 'r')) {

            $this->logger->err(__METHOD__ . "File [$filename] not found");

            throw new \RuntimeException("File [$filename] not found");
        }

        $headers = fgetcsv($handle, 1000, ",", "'");

        while ($row = fgetcsv($handle, 1000, ",", "'")) {

            $data = array_combine($headers, $row);

            $timeFields = ['created_at', 'updated_at', 'deleted_at'];

            foreach ($timeFields as $field) {

                unset($data[$field]);

            }

            $channelRow = new ChannelModel();

            foreach ($data as $key => $value) {
                $channelRow->$key = $value;
            }

            $channelRow->save();

        }
    }

    /**
     * Sets up the database tables
     */
    public function setupSchema()
    {
        $this->logger->debug(__METHOD__ . ' : bof');

        Capsule::schema()->create('service_livetv_channel', function ($table) {
            $table->increments('id');

            $table->string('uuid', 36)->unique();

            $table->integer('source_id')->unsigned;

            $table->string('short_name', 30)->unique();

            $table->string('full_name', 128);

            $table->string('time_zone', 30);

            $table->string('primary_language', 2)->nullable();

            $table->integer('weight');

            $table->timestamps();

            $table->softDeletes();
        });

        $this->logger->debug(__METHOD__ . ' : Created `service_livetv_channel`');

        Capsule::schema()->create('service_livetv_program', function ($table) {
            $table->increments('id');

            $table->bigInteger('ext_program_id')->unsigned();

            $table->enum('show_type', ['movie', 'series', 'other'])->nullable()->default('other');

            $table->string('long_title', 255);

            $table->string('grid_title', 15)->nullable()->default(null);

            $table->string('original_title', 255)->nullable()->default(null);

            $table->integer('duration')->nullable()->default(null);

            $table->string('iso_2_lang', 2)->nullable()->default(null);

            $table->string('eidr_id', 50)->nullable()->default(null);

            $table->timestamps();

            $table->softDeletes();

            $table->index('ext_program_id', 'indx_ext_program_id');

            $table->index('long_title', 'indx_long_title');
        });

        $this->logger->debug(__METHOD__ . ' : Created `service_livetv_program`');

        Capsule::schema()->create('service_livetv_schedule', function ($table) {
            $table->increments('id');

            $table->bigInteger('ext_schedule_id')->unsigned()->unique();

            $table->integer('channel_id')->unsigned();

            $table->integer('start_time')->unsigned();

            $table->integer('end_time')->unsigned();

            $table->integer('run_time')->unsigned();

            $table->integer('program_id')->unsigned();

            $table->boolean('is_live');

            $table->timestamps();

            $table->softDeletes();

            $table->foreign('channel_id')
                ->references('id')->on('service_livetv_channel')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('program_id')
                ->references('id')->on('service_livetv_program')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        $this->logger->debug(__METHOD__ . ' : Created `service_livetv_schedule`');
    }


}