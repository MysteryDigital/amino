# Readme

I decided to approach this project from scratch rather than using an existing framework.  I thought that this gave me a chance
to use design patterns in a more clear way.  It may well be better/faster/cheaper to use a framework like Lumen for something like
this but I wanted to demonstrate that I understand IoC and other design patterns.

Doing it from scratch leaves some obvious problems like not having the time to code all the stuff around database migrations which
would normally be present in a full framework.

Code adheres to PSR2.

## Setup
Please run `composer install` to fetch the dependencies.

You will need to create a database called amino_project and then grant permissions to it, thusly:

```
grant all privileges on amino_project.* to `amino`@`localhost` identified by 'c02f88fd-7078-4ca2-800e-974b34a58e23';
```

Edit `config/database.php` to suit your environment.

Then run `php refresh_database.php` to set up the database.

You run the program with `php main.php`

## Testing
I have not coded any integration tests because I only spent the
weekend on this project.  The amount of functions that actually need
to be tested is quite small though as a lot of the complicated functionality
is provided by libraries that are tested and trusted.

## Dependency injection
I'm using Pimple to provide an inversion of control container so as to make it easier to decouple my dependencies.  This 
allows you to swap out service providers without needing to address how they're made available in the application logic.
Wherever possible I'm using libraries that provide functionality that is as close as possible to other libraries.  For example
Monolog adheres to PSR3 and you can drop any other PSR3 compliant object in its place without making any code adjustments.

## Import Strategies
Although this is a pretty simple project I used the strategy pattern to allow the main Amino class to be able to swap
between different ways of importing data.  

Import strategies must implement the interface defined in App\Interfaces\ImportStrategyInterface.php

The method they implement must be a generator that returns rows to be inserted into the database.  A key called "type"
gives an indication which table the data must be inserted into.  A key called "payload" contains an
associative array of key-value pairs corresponding to the database field names.

```
[
  "type" => 'schedule' or 'program',
  "payload" => [
    'database field name' => 'value',#
    'database field name' => 'value',
    'database field name' => 'value',
  ],
]
```

## Config
This project uses a standard config package.  
All files in the config directory will be processed.

You can find examples of working configuration files [here](https://github.com/hassankhan/config/blob/master/tests/mocks/pass).

## Database
This project uses the Laravel database package.  It currently supports MySQL, Postgres, SQL Server, and SQLite.
By default I'm using MySQL since the supplied schema and I know that Amino uses it inhouse.
 
You can configure the database settings in the config/database.php file

The database object isn't supplied as a dependency because all of the ORM packages
are so opinionated about how they are accessed.  Rather we use the repository pattern
to decouple our persistence layer from our domain logic.  It's kind of a second prize but there we are.

To setup the database run the `refresh_database.php` script.

## Repository pattern
This project is not a great place to use the repository pattern because there is not a lot of
domain logic.  I implemented it just because I mentioned in our conversation.

In a more complicated data environment all of the domain logic would rest in services.  The entities are in direct
contact with the persistance layer and should only have logic about relationships and possibly filters.  The repositories
use the entities to get raw data and they format it (sort, collate, filter, etc) for the service layer.  The service layer
is entirely agnostic of the persistance layer and you should be able to use an entirely different ORM without needing to
rewrite your business rules that are contained in the service layer.

It's really a bad fit for something like this, but it's no effort to set up so that it can be extended on in the future.